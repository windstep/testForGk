<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Форма быстрого заказа</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="content" id="app">
    <div class="container">
        <form class="row justify-content-center mt-5" action="{{action('OrderController@send')}}" method="POST">
            {{csrf_field()}}
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Форма быстрой заявки</div>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul class="mb-0">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (session()->has('status'))
                            <div class="alert alert-success">
                                <ul class="mb-0">
                                    <li>{{ session('status') }}</li>
                                </ul>
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="name">Имя</label>
                            <input name="name" id="name" class="form-control" required type="text" placeholder="Имя">
                        </div>
                        <div class="form-group">
                            <label for="phone">Телефон</label>
                            <input name="phone" class="form-control" required type="text"
                                   placeholder="8 (888) 888-88-88">
                        </div>
                        <div class="form-group">
                            <label for="phone">Дата начала заезда</label>
                            <datepicker name="start" input-class="form-control" :required="true" :language="ru"
                                        format="dd.MM.yyyy"></datepicker>

                        </div>
                        <div class="form-group">
                            <label for="phone">Дата начала заезда</label>
                            <datepicker name="finish" input-class="form-control" :required="true" :language="ru"
                                        format="dd.MM.yyyy"></datepicker>
                        </div>
                        <button class="btn btn-outline-primary">Отправить</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
