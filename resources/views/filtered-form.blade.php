<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Форма быстрого заказа</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div id="app" class="content p-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session()->has('status'))
                    <div class="alert alert-success">
                        <ul class="mb-0">
                            <li>{{ session('status') }}</li>
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Фильтрация объектов</div>
                    </div>
                    <form method="get">
                        <div class="card-body">
                            <span class="mb-2 d-block">Количество звезд</span>

                            <label>
                                <input name="filter" value="1" type="radio" {{request()->input('filter') == 1 ? 'checked' : ''}}>
                                1
                            </label>
                            <label>
                                <input name="filter" value="2" type="radio" {{request()->input('filter') == 2 ? 'checked' : ''}}>
                                2
                            </label>
                            <label>
                                <input name="filter" value="3" type="radio" {{request()->input('filter') == 3 ? 'checked' : ''}}>
                                3
                            </label>
                            <label>
                                <input name="filter" value="4" type="radio" {{request()->input('filter') == 4 ? 'checked' : ''}}>
                                4
                            </label>
                            <label>
                                <input name="filter" value="5" type="radio" {{request()->input('filter') == 5 ? 'checked' : ''}}>
                                5
                            </label>

                            <div class="form-group">
                                <label for="sort_parameter">Фильтровать по:</label>
                                <select name="sort_parameter" id="sort_parameter" class="form-control">
                                    <option value="name" {{!request()->has('sort_parameter') || request()->input('sort_parameter') == 'name' ? 'selected' : ''}}>
                                        По алфавиту
                                    </option>
                                    <option value="star" {{!request()->has('sort_parameter') || request()->input('sort_parameter') == 'star' ? 'selected' : ''}}>
                                        По звездам
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sort_direction">Вид сортировки:</label>
                                <select name="sort_direction" id="sort_direction" class="form-control">
                                    <option value="asc" {{!request()->has('sort_direction') || request()->input('sort_direction') == 'asc' ? 'selected' : ''}}>
                                        от меньшего к большему
                                    </option>
                                    <option value="desc" {{(request()->has('sort_direction') && request()->input('sort_direction') == 'desc') ? 'selected' : ''}}>
                                        от большего к меньшему
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary btn-block">Фильтровать</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Список всех объектов</div>
                    </div>
                    <div class="card-body">
                        <div class="elements">
                            <ul>
                                @foreach ($elements as $element)
                                    <li>{{$element->meta->name}} <i class="fa fa-star"></i> {{$element->meta->star}}
                                    </li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>