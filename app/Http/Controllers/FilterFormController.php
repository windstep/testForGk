<?php

namespace App\Http\Controllers;

use App\Models\PlaceApi;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class FilterFormController extends Controller
{
    public function index(Request $request)
    {
        try {
            $queryObject = new Client();
            $response = $queryObject->get('https://adm.zdravproduct.com/api/v1/objects/', ['verify' => false]);
        } catch (\Exception $e) {
            return view('filtered-form')->with(['errors' => ['query' => 'К сожалению, у нас не получилось вытащить данные из сервиса:(']]);
        }

        $dataObject = json_decode($response->getBody()->getContents(), true);
        $elementsCollection = collect();
        foreach ($dataObject['data'] as $dataPiece) {
            $elementsCollection->push(new PlaceApi($dataPiece));
        }

        if ($request->has('filter')) {
            $elementsCollection = $elementsCollection->where('meta.star', $request->input('filter'));
        }

        if ($request->has('sort_parameter') && $request->input('sort_parameter') == 'star') {
            if ($request->has('sort_direction') && $request->input('sort_direction') == 'desc') {
                $filteredCollection = $elementsCollection->sortByDesc(function ($el, $i) {
                    return $el->meta->star;
                });
            } else {
                $filteredCollection = $elementsCollection->sortBy(function ($el, $i) {
                    return $el->meta->star;
                });
            }
        } else {
            if ($request->has('sort_direction') && $request->input('sort_direction') == 'desc') {
                $filteredCollection = $elementsCollection->sortByDesc(function ($el, $i) {
                    return preg_replace('/[СC]анаторий /ui', '', $el->meta->name);
                });
            } else {
                $filteredCollection = $elementsCollection->sortBy(function ($el, $i) {
                    return preg_replace('/[СC]анаторий /ui', '', $el->meta->name);
                });
            }
        }

        return view('filtered-form')->with('elements', $filteredCollection);
    }
}
