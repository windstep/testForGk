<?php

namespace App\Http\Controllers;

use App\Models\OrderApi;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use App\Http\Requests\Order as OrderRequest;

class OrderController extends Controller
{
    public function index()
    {
        return view('fast_order-form');
    }

    public function send(OrderRequest $request)
    {
        $orderApi = new OrderApi($request->all());
        $guzzleClient = new Client();
        try {
            $response = $guzzleClient->request('POST', 'https://ko.tour-shop.ru/siteLead', [
                'query' => [
                    'site_id' => 100,
                    'type' => 'order'
                ],
                'form_params' => [
                    'data' => $orderApi->toArray()
                ],
                'headers' => [
                    'KoSiteKey' => 'test198'
                ]
            ]);
        } catch (GuzzleException $e) {
            return redirect()->back()->with(['errors' => ['data' => $e->getMessage()]]);
        }

        $responseText = $response->getBody()->getContents();

        if (stripos($responseText, 'error')) {
            return redirect()->back()->with(['errors' => ['data' => 'Внутренняя ошибка передачи данных']]);
        }
        return redirect()->back()
            ->with(['status' => 'Успешно переданы данные: номер заявки ' . substr($responseText, strpos($responseText, '=') + 1)]);
    }
}
