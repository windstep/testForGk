<?php

namespace App\Models;

use Jenssegers\Model\Model;

class PlaceApi extends Model
{
    protected $casts = [
        'id' => 'integer',
        'uid' => 'string',
        'meta' => 'object',
        'rewards' => 'object',
        'therapy' => 'object',
        'profiles' => 'object',
        'services' => 'object',
        'rooms' => 'object',
        'pages' => 'object',
        'images' => 'object'
    ];
}