<?php

namespace App\Models;

use Carbon\Carbon;
use Jenssegers\Model\Model;

class OrderApi extends Model
{
    protected $casts = [
        'name' => 'string',
        'phone' => 'string',
        'start' => 'datetime',
        'finish' => 'datetime'
    ];

    protected $fillable = ['name', 'phone', 'start', 'finish'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function toArray()
    {
        return [
            [
                'name' => 'Имя',
                'value' => $this->name
            ],
            [
                'name' => 'Телефон',
                'value' => $this->phone
            ],
            [
                'name' => 'Дата заезда',
                'value' => $this->start
            ],
            [
                'name' => 'Дата выезда',
                'value' => $this->finish
            ]
        ];
    }

    public function getStartAttribute($attr)
    {
        return Carbon::parse($attr)->format('d.m.Y');
    }

    public function getFinishAttribute($attr)
    {
        return Carbon::parse($attr)->format('d.m.Y');
    }
}